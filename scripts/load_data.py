#!/us/bin/env python3
"""
Functions to manipulate data fror bilio
"""

from os import walk, path, listdir
from datetime import datetime
import re
from difflib import SequenceMatcher
import shutil

from pandas import DataFrame, read_csv
from openpyxl import load_workbook
import numpy as np


DATA_FOLDER = path.join(path.dirname(__file__), "../data_from_saclay/fondsBiblio")
DATA_DIR = path.join(path.dirname(__file__), "../data_from_saclay/tmp/")

BOOK_FILE = path.join(path.dirname(__file__), "../data/book_list.csv")

CATE2COTE_FILE2 = path.join(path.dirname(__file__), "../data/cate2cote_from_fond.csv")
CATE2COTE_FILE = path.join(path.dirname(__file__), "../data/cate2cote.csv")

CATE2COTE = {}


def get_date(file_name):
    """ Get date from file name """
    exp = re.compile(r"^(?P<year>\d{4})[- _](?P<month>\d{2})[- _](?P<day>\d{2}).*")
    exp2 = re.compile(r"^(?P<day>\d{2})[- _](?P<month>\d{2})[- _](?P<year>\d{4}).*")

    reg = exp.match(file_name)
    if reg is None:
        reg = exp2.match(file_name)

    date = datetime(int(reg.group("year")),
                    int(reg.group("month")),
                    int(reg.group("day")))

    return date


def extract_data(file_name):
    """ Extracting data from Excel file """

    from itertools import islice
    wb = load_workbook(file_name)
    ws = wb.active
    data = ws.values
    cols = next(data)[1:]
    data = list(data)
    idx = [r[0] for r in data]
    data = (islice(r, 1, None) for r in data)
    df = DataFrame(data, index=idx, columns=cols)

    base, _ = path.splitext(file_name)

    new_name = path.join(DATA_DIR, base+".csv")
    df.to_csv(base+".csv")

    return df


def reorder():
    """ Reorder files """
    for root, _, files in walk(DATA_FOLDER, topdown=False):
        for name in files:
            if name.endswith("ods") or \
               name.endswith("xls") or \
               name.endswith("xlsx"):
                my_date = get_date(name)
                _, ext = path.splitext(path.basename(name))
                new_name = path.join(DATA_DIR, my_date.strftime("%Y-%m-%d")+ext)
                shutil.copyfile(path.join(root, name),
                                new_name)


def correct_df(df):
    """ Apply correction on data frame (renaming...) """
    # Looking for identical title
    length = len(df.values)

    if CATE2COTE == {}:
        build_cate2cote(file_name=CATE2COTE_FILE)

    threshold = 0.75
    i = 0
    for frame in df.values:
        i += 1
        #print("line", i)
        #print(frame)

        iid = frame[0]
        title = frame[1]
        authors = frame[2]
        editor = frame[3]
        cate = frame[4]
        cote = frame[5]
        isbn = frame[6]

        if not 0 <= int(iid) <= 20000:
            print("ID: Issue on line {}".format(i))

        if not isinstance(title, str) or title == "":
            print("{} TITLE: Missing".format(iid))

        if not isinstance(authors, str) or authors == "":
            print("{} AUTHOR: Missing".format(iid))

        if not isinstance(editor, str) or editor == "":
            if int(iid) >= 13000:
                print("{} EDITOR: Missing".format(iid))

        if not isinstance(isbn, str) or len(isbn) != 13:
            if isinstance(isbn, str) and len(isbn) == 10:
                #TODO: correct by adding 978 at the beginning
                pass
            else:
                if isinstance(isbn, str) and isbn == "0":
                    pass
                else:
                    #print("{} ISBN: Non correct value: {}".format(iid, isbn))
                    pass

        # Check on category and cote

        if not isinstance(cote, str):
            print("{} COTE: Missing cote".format(iid))
            #TODO: build cote
        else:
            if cate not in CATE2COTE:
                code = cote.split(" ")[0]
                real_cate = None
                for key, val in CATE2COTE.items():
                    if val == code:
                        real_cate = key
                        break
                if real_cate is not None:
                    print("{} CATE: Wrong category is '{}' should be '{}'"\
                          .format(iid, cate, real_cate))
                else:
                    print("{} CODE: Could not found code in cate2code {}"\
                          .format(iid, code))
            else:
                # Checking cote match
                comp_cote = build_cote(frame)
                comp_cote2 = build_cote(frame, reverse_author=True)
                #print("comp_cotes", comp_cote, comp_cote2)
                # For BD and BDs ignoring BD vs BDs comparing juste Leters
                # Temporary TODO: remove
                if cote.startswith("AN") or cote.startswith("AL") \
                   or cote.startswith("ES"):
                    continue
                if "BD " in cote or "BDs " in cote or cote.startswith("E"):
                    comp = comp_cote.split(" ")[1]
                    comp2 = comp_cote2.split(" ")[1]
                    mycote = cote.split(" ")[1]
                    if mycote not in [comp, comp2]:
                        print("{} COTE BD: original: '{}' computed: '{}'"\
                              .format(iid, cote, comp_cote))
                else:
                    if cote not in [comp_cote, comp_cote2]:
                        print("{} COTE: original: '{}' computed: '{}' or '{}'"\
                              .format(iid, cote, comp_cote, comp_cote2))

       ## Look for indentical title, author, cate with a misspell
       #for frame2 in df.values[i:]:
       #    iid2 = frame2[0]

       #    title2 = frame2[1]
       #    if not isinstance(title, str) or not isinstance(title2, str):
       #        continue
       #    if title == title2:
       #        continue
       #    ratio = SequenceMatcher(a=title, b=title2).ratio()
       #    if ratio >= threshold:
       #        print("{} {} TITLE: those two titles are similar {}:\n{}\n{}\n"\
       #              .format(iid, iid2, ratio, title, title2))
       #    author2 = frame2[2]
       #    if not isinstance(author, str) or not isinstance(author2, str):
       #        continue
       #    if author == author2:
       #        continue
       #    ratio = SequenceMatcher(a=author, b=author2).ratio()
       #    if ratio >= threshold:
       #        print("{} {} AUTHOR: those two authors are similar {}:\n{}\n{}\n"\
       #              .format(iid, iid2, ratio, author, author2))


def build_cate2cote(df=None, file_name=None):

    if df is not None:
        conv = {}
        for frame in df.values:
            cate = frame[4]
            if not isinstance(frame[5], str):
                continue
            cote = frame[5].split(" ")[0]

            if cate in conv:
                if cote not in conv[cate]:
                    conv[cate].append(cote)
                    print(frame[0], cote)
            else:
                conv[cate] = [cote]

        with open(file_name, "w") as ffile:
            ffile.write("#Categorie,cote\n")
            for cate in sorted(conv.keys()):
                cote = conv[cate]
                if len(cote) > 1:
                    print("issue with:\ncate {}\ncote {}"\
                          .format(cate, ", ".join(cote)))
                ffile.write("{},{}\n".format(cate, cote[0]))
    else:
        with open(file_name, "r") as ffile:
            for line in ffile.readlines():
                print(line)
                if line[0] == "#":
                    continue
                cate, cote = line[:-1].split(";")
                CATE2COTE[cate] = cote

        from pprint import pprint
        pprint(CATE2COTE)


def build_cote(frame, reverse_author=False):
    """ Build cote frome book data """
    cate = frame[4]
    title = frame[1]
    author = frame[2]

    if cate.lower() in ["manga", "bd", "bds"] or \
       (cate.lower() == "manga" and int(frame[0]) >= 15000):
        if cate.lower() == "manga":
            cote = "03 "
        elif "T0" in title or \
             "T1" in title or \
             "T2" in title or \
             "T3" in title or \
             "T4" in title or \
             "T5" in title or \
             "T6" in title or \
             "T7" in title or \
             "T8" in title or \
             "T9" in title:
            cote = "BDs "
        else:
            cote = "BD "

        tmp = title.upper().replace(" ", "")
        for article in ["les ", "le ", "la ", "un ", "une ", "l'", "the"]:

            if title.lower().startswith(article):
                tmp = title.upper()
                tmp = tmp.replace(article.upper(), "", 1).replace(" ", "")
                break
        if title[:4] == "RG T":
            tmp = "RG "
        if title[:4] == "Il é":
            tmp = "IL "

        cote += tmp.replace("'", "").replace(".", "")[:3].strip()

    else:
        # Get name of first author sometimes name and surname are reversed
        tmp = author.split(";")[0].strip()
        if author == "Van Cauwelaert Didier":
            tmp = "CAU"
        elif author == "Oe Kenzaburo":
            tmp = "OE "
        elif author == "Bâ Mariama":
            tmp = "BA "
        elif author == "Lê Linda":
            tmp = "LE "
        elif author == "Sa Shan":
            tmp = "SA "
        elif author == "La Bruyere":
            tmp = "LAB"
        elif author == "Dos Passos John":
            tmp = "PAS"
        elif author == "Li Yu":
            tmp = "LIY"
        elif author == "Le Tasse":
            tmp = "LET"
        elif author == "Ou Tcheng'En":
            tmp = "OUT"
        elif author == "Ya Ding":
            tmp = "YAD"
        elif author == "Wu Cheng'En":
            tmp = "WUC"
        elif author == "Cars (Des) Laurence":
            tmp = "DES"
        elif author == "La Galite":
            tmp = "LAG"
        else:
            idx = 0 if reverse_author else -1
            sep = tmp.split(" ")
            if len(sep) == 1:
                idx = 0
            tmp = sep[idx]
            if len(sep) > 2 and len(sep[idx]) < 3:
                tmp += sep[1 if idx == 0 else -2]
            tmp = tmp.replace("'", "")
        cote = CATE2COTE[cate] + " " + tmp[:3].upper()

    cote = cote.replace("È", "E")\
               .replace("É", "E")\
               .replace("Ç", "C")\
               .replace("Î", "I")\
               .replace("Ä", "A")\
               .replace("Ü", "U")\
               .replace("Â", "A")\
               .replace("Ï", "I")\
               .strip()

    return cote


if __name__ == "__main__":
    #reorder()
    #df = extract_data(path.join(data_dir2, "2022-04-26.xlsx"))
    #ddir = "/home/yugi/biblio-saclay/data_from_saclay/tmp"
    #for ffile in listdir(ddir):
    #    df = extract_data(path.join(ddir, ffile))

    df = read_csv(BOOK_FILE)
    # Still to do
    correct_df(df)

    #build_cate2cote(df=df, file_name=CATE2COTE_FILE2)
